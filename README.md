# Harry Potter Trading Card Game - Web game

## Development

Use `yarn` instead `npm`.

### Testing

To make testing PIXI (canvas) available through Jest, package `canvas` needs to be installed.

To install `canvas` [requirements](https://www.npmjs.com/package/canvas#installation) must be met.
