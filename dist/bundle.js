(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(factory());
}(this, (function () { 'use strict';

function install (store, resources) {
  store.textures = {};

  for (const name in resources) {
    store.textures[name] = PIXI.Texture.from(resources[name].url);
  }
}

function loaderProgress (loader, resource) {
  console.log(`Loading: "${resource.name}"`);
  console.log(`Progress: ${loader.progress}%`);
}

function load (store, resources) {
  let toLoad = [];
  for (const resource in resources) {
    toLoad = toLoad.concat(resources[resource]);
  }

  const loader = new PIXI.loaders.Loader();
  loader.onProgress.add(loaderProgress);
  loader.add(toLoad);

  return new Promise((resolve) => {
    loader.load((loader, resources) => {
      install(store, resources);
      resolve();
    });
  });
}

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var eventemitter2 = createCommonjsModule(function (module, exports) {
/*!
 * EventEmitter2
 * https://github.com/hij1nx/EventEmitter2
 *
 * Copyright (c) 2013 hij1nx
 * Licensed under the MIT license.
 */
!function(undefined) {

  var isArray = Array.isArray ? Array.isArray : function _isArray(obj) {
    return Object.prototype.toString.call(obj) === "[object Array]";
  };
  var defaultMaxListeners = 10;

  function init() {
    this._events = {};
    if (this._conf) {
      configure.call(this, this._conf);
    }
  }

  function configure(conf) {
    if (conf) {
      this._conf = conf;

      conf.delimiter && (this.delimiter = conf.delimiter);
      this._maxListeners = conf.maxListeners !== undefined ? conf.maxListeners : defaultMaxListeners;

      conf.wildcard && (this.wildcard = conf.wildcard);
      conf.newListener && (this._newListener = conf.newListener);
      conf.removeListener && (this._removeListener = conf.removeListener);
      conf.verboseMemoryLeak && (this.verboseMemoryLeak = conf.verboseMemoryLeak);

      if (this.wildcard) {
        this.listenerTree = {};
      }
    } else {
      this._maxListeners = defaultMaxListeners;
    }
  }

  function logPossibleMemoryLeak(count, eventName) {
    var errorMsg = '(node) warning: possible EventEmitter memory ' +
        'leak detected. ' + count + ' listeners added. ' +
        'Use emitter.setMaxListeners() to increase limit.';

    if(this.verboseMemoryLeak){
      errorMsg += ' Event name: ' + eventName + '.';
    }

    if(typeof process !== 'undefined' && process.emitWarning){
      var e = new Error(errorMsg);
      e.name = 'MaxListenersExceededWarning';
      e.emitter = this;
      e.count = count;
      process.emitWarning(e);
    } else {
      console.error(errorMsg);

      if (console.trace){
        console.trace();
      }
    }
  }

  function EventEmitter(conf) {
    this._events = {};
    this._newListener = false;
    this._removeListener = false;
    this.verboseMemoryLeak = false;
    configure.call(this, conf);
  }
  EventEmitter.EventEmitter2 = EventEmitter; // backwards compatibility for exporting EventEmitter property

  //
  // Attention, function return type now is array, always !
  // It has zero elements if no any matches found and one or more
  // elements (leafs) if there are matches
  //
  function searchListenerTree(handlers, type, tree, i) {
    if (!tree) {
      return [];
    }
    var listeners=[], leaf, len, branch, xTree, xxTree, isolatedBranch, endReached,
        typeLength = type.length, currentType = type[i], nextType = type[i+1];
    if (i === typeLength && tree._listeners) {
      //
      // If at the end of the event(s) list and the tree has listeners
      // invoke those listeners.
      //
      if (typeof tree._listeners === 'function') {
        handlers && handlers.push(tree._listeners);
        return [tree];
      } else {
        for (leaf = 0, len = tree._listeners.length; leaf < len; leaf++) {
          handlers && handlers.push(tree._listeners[leaf]);
        }
        return [tree];
      }
    }

    if ((currentType === '*' || currentType === '**') || tree[currentType]) {
      //
      // If the event emitted is '*' at this part
      // or there is a concrete match at this patch
      //
      if (currentType === '*') {
        for (branch in tree) {
          if (branch !== '_listeners' && tree.hasOwnProperty(branch)) {
            listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], i+1));
          }
        }
        return listeners;
      } else if(currentType === '**') {
        endReached = (i+1 === typeLength || (i+2 === typeLength && nextType === '*'));
        if(endReached && tree._listeners) {
          // The next element has a _listeners, add it to the handlers.
          listeners = listeners.concat(searchListenerTree(handlers, type, tree, typeLength));
        }

        for (branch in tree) {
          if (branch !== '_listeners' && tree.hasOwnProperty(branch)) {
            if(branch === '*' || branch === '**') {
              if(tree[branch]._listeners && !endReached) {
                listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], typeLength));
              }
              listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], i));
            } else if(branch === nextType) {
              listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], i+2));
            } else {
              // No match on this one, shift into the tree but not in the type array.
              listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], i));
            }
          }
        }
        return listeners;
      }

      listeners = listeners.concat(searchListenerTree(handlers, type, tree[currentType], i+1));
    }

    xTree = tree['*'];
    if (xTree) {
      //
      // If the listener tree will allow any match for this part,
      // then recursively explore all branches of the tree
      //
      searchListenerTree(handlers, type, xTree, i+1);
    }

    xxTree = tree['**'];
    if(xxTree) {
      if(i < typeLength) {
        if(xxTree._listeners) {
          // If we have a listener on a '**', it will catch all, so add its handler.
          searchListenerTree(handlers, type, xxTree, typeLength);
        }

        // Build arrays of matching next branches and others.
        for(branch in xxTree) {
          if(branch !== '_listeners' && xxTree.hasOwnProperty(branch)) {
            if(branch === nextType) {
              // We know the next element will match, so jump twice.
              searchListenerTree(handlers, type, xxTree[branch], i+2);
            } else if(branch === currentType) {
              // Current node matches, move into the tree.
              searchListenerTree(handlers, type, xxTree[branch], i+1);
            } else {
              isolatedBranch = {};
              isolatedBranch[branch] = xxTree[branch];
              searchListenerTree(handlers, type, { '**': isolatedBranch }, i+1);
            }
          }
        }
      } else if(xxTree._listeners) {
        // We have reached the end and still on a '**'
        searchListenerTree(handlers, type, xxTree, typeLength);
      } else if(xxTree['*'] && xxTree['*']._listeners) {
        searchListenerTree(handlers, type, xxTree['*'], typeLength);
      }
    }

    return listeners;
  }

  function growListenerTree(type, listener) {

    type = typeof type === 'string' ? type.split(this.delimiter) : type.slice();

    //
    // Looks for two consecutive '**', if so, don't add the event at all.
    //
    for(var i = 0, len = type.length; i+1 < len; i++) {
      if(type[i] === '**' && type[i+1] === '**') {
        return;
      }
    }

    var tree = this.listenerTree;
    var name = type.shift();

    while (name !== undefined) {

      if (!tree[name]) {
        tree[name] = {};
      }

      tree = tree[name];

      if (type.length === 0) {

        if (!tree._listeners) {
          tree._listeners = listener;
        }
        else {
          if (typeof tree._listeners === 'function') {
            tree._listeners = [tree._listeners];
          }

          tree._listeners.push(listener);

          if (
            !tree._listeners.warned &&
            this._maxListeners > 0 &&
            tree._listeners.length > this._maxListeners
          ) {
            tree._listeners.warned = true;
            logPossibleMemoryLeak.call(this, tree._listeners.length, name);
          }
        }
        return true;
      }
      name = type.shift();
    }
    return true;
  }

  // By default EventEmitters will print a warning if more than
  // 10 listeners are added to it. This is a useful default which
  // helps finding memory leaks.
  //
  // Obviously not all Emitters should be limited to 10. This function allows
  // that to be increased. Set to zero for unlimited.

  EventEmitter.prototype.delimiter = '.';

  EventEmitter.prototype.setMaxListeners = function(n) {
    if (n !== undefined) {
      this._maxListeners = n;
      if (!this._conf) this._conf = {};
      this._conf.maxListeners = n;
    }
  };

  EventEmitter.prototype.event = '';


  EventEmitter.prototype.once = function(event, fn) {
    return this._once(event, fn, false);
  };

  EventEmitter.prototype.prependOnceListener = function(event, fn) {
    return this._once(event, fn, true);
  };

  EventEmitter.prototype._once = function(event, fn, prepend) {
    this._many(event, 1, fn, prepend);
    return this;
  };

  EventEmitter.prototype.many = function(event, ttl, fn) {
    return this._many(event, ttl, fn, false);
  };

  EventEmitter.prototype.prependMany = function(event, ttl, fn) {
    return this._many(event, ttl, fn, true);
  };

  EventEmitter.prototype._many = function(event, ttl, fn, prepend) {
    var self = this;

    if (typeof fn !== 'function') {
      throw new Error('many only accepts instances of Function');
    }

    function listener() {
      if (--ttl === 0) {
        self.off(event, listener);
      }
      return fn.apply(this, arguments);
    }

    listener._origin = fn;

    this._on(event, listener, prepend);

    return self;
  };

  EventEmitter.prototype.emit = function() {

    this._events || init.call(this);

    var type = arguments[0];

    if (type === 'newListener' && !this._newListener) {
      if (!this._events.newListener) {
        return false;
      }
    }

    var al = arguments.length;
    var args,l,i,j;
    var handler;

    if (this._all && this._all.length) {
      handler = this._all.slice();
      if (al > 3) {
        args = new Array(al);
        for (j = 0; j < al; j++) args[j] = arguments[j];
      }

      for (i = 0, l = handler.length; i < l; i++) {
        this.event = type;
        switch (al) {
        case 1:
          handler[i].call(this, type);
          break;
        case 2:
          handler[i].call(this, type, arguments[1]);
          break;
        case 3:
          handler[i].call(this, type, arguments[1], arguments[2]);
          break;
        default:
          handler[i].apply(this, args);
        }
      }
    }

    if (this.wildcard) {
      handler = [];
      var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
      searchListenerTree.call(this, handler, ns, this.listenerTree, 0);
    } else {
      handler = this._events[type];
      if (typeof handler === 'function') {
        this.event = type;
        switch (al) {
        case 1:
          handler.call(this);
          break;
        case 2:
          handler.call(this, arguments[1]);
          break;
        case 3:
          handler.call(this, arguments[1], arguments[2]);
          break;
        default:
          args = new Array(al - 1);
          for (j = 1; j < al; j++) args[j - 1] = arguments[j];
          handler.apply(this, args);
        }
        return true;
      } else if (handler) {
        // need to make copy of handlers because list can change in the middle
        // of emit call
        handler = handler.slice();
      }
    }

    if (handler && handler.length) {
      if (al > 3) {
        args = new Array(al - 1);
        for (j = 1; j < al; j++) args[j - 1] = arguments[j];
      }
      for (i = 0, l = handler.length; i < l; i++) {
        this.event = type;
        switch (al) {
        case 1:
          handler[i].call(this);
          break;
        case 2:
          handler[i].call(this, arguments[1]);
          break;
        case 3:
          handler[i].call(this, arguments[1], arguments[2]);
          break;
        default:
          handler[i].apply(this, args);
        }
      }
      return true;
    } else if (!this._all && type === 'error') {
      if (arguments[1] instanceof Error) {
        throw arguments[1]; // Unhandled 'error' event
      } else {
        throw new Error("Uncaught, unspecified 'error' event.");
      }
      return false;
    }

    return !!this._all;
  };

  EventEmitter.prototype.emitAsync = function() {

    this._events || init.call(this);

    var type = arguments[0];

    if (type === 'newListener' && !this._newListener) {
        if (!this._events.newListener) { return Promise.resolve([false]); }
    }

    var promises= [];

    var al = arguments.length;
    var args,l,i,j;
    var handler;

    if (this._all) {
      if (al > 3) {
        args = new Array(al);
        for (j = 1; j < al; j++) args[j] = arguments[j];
      }
      for (i = 0, l = this._all.length; i < l; i++) {
        this.event = type;
        switch (al) {
        case 1:
          promises.push(this._all[i].call(this, type));
          break;
        case 2:
          promises.push(this._all[i].call(this, type, arguments[1]));
          break;
        case 3:
          promises.push(this._all[i].call(this, type, arguments[1], arguments[2]));
          break;
        default:
          promises.push(this._all[i].apply(this, args));
        }
      }
    }

    if (this.wildcard) {
      handler = [];
      var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
      searchListenerTree.call(this, handler, ns, this.listenerTree, 0);
    } else {
      handler = this._events[type];
    }

    if (typeof handler === 'function') {
      this.event = type;
      switch (al) {
      case 1:
        promises.push(handler.call(this));
        break;
      case 2:
        promises.push(handler.call(this, arguments[1]));
        break;
      case 3:
        promises.push(handler.call(this, arguments[1], arguments[2]));
        break;
      default:
        args = new Array(al - 1);
        for (j = 1; j < al; j++) args[j - 1] = arguments[j];
        promises.push(handler.apply(this, args));
      }
    } else if (handler && handler.length) {
      handler = handler.slice();
      if (al > 3) {
        args = new Array(al - 1);
        for (j = 1; j < al; j++) args[j - 1] = arguments[j];
      }
      for (i = 0, l = handler.length; i < l; i++) {
        this.event = type;
        switch (al) {
        case 1:
          promises.push(handler[i].call(this));
          break;
        case 2:
          promises.push(handler[i].call(this, arguments[1]));
          break;
        case 3:
          promises.push(handler[i].call(this, arguments[1], arguments[2]));
          break;
        default:
          promises.push(handler[i].apply(this, args));
        }
      }
    } else if (!this._all && type === 'error') {
      if (arguments[1] instanceof Error) {
        return Promise.reject(arguments[1]); // Unhandled 'error' event
      } else {
        return Promise.reject("Uncaught, unspecified 'error' event.");
      }
    }

    return Promise.all(promises);
  };

  EventEmitter.prototype.on = function(type, listener) {
    return this._on(type, listener, false);
  };

  EventEmitter.prototype.prependListener = function(type, listener) {
    return this._on(type, listener, true);
  };

  EventEmitter.prototype.onAny = function(fn) {
    return this._onAny(fn, false);
  };

  EventEmitter.prototype.prependAny = function(fn) {
    return this._onAny(fn, true);
  };

  EventEmitter.prototype.addListener = EventEmitter.prototype.on;

  EventEmitter.prototype._onAny = function(fn, prepend){
    if (typeof fn !== 'function') {
      throw new Error('onAny only accepts instances of Function');
    }

    if (!this._all) {
      this._all = [];
    }

    // Add the function to the event listener collection.
    if(prepend){
      this._all.unshift(fn);
    }else{
      this._all.push(fn);
    }

    return this;
  };

  EventEmitter.prototype._on = function(type, listener, prepend) {
    if (typeof type === 'function') {
      this._onAny(type, listener);
      return this;
    }

    if (typeof listener !== 'function') {
      throw new Error('on only accepts instances of Function');
    }
    this._events || init.call(this);

    // To avoid recursion in the case that type == "newListeners"! Before
    // adding it to the listeners, first emit "newListeners".
    if (this._newListener)
       this.emit('newListener', type, listener);

    if (this.wildcard) {
      growListenerTree.call(this, type, listener);
      return this;
    }

    if (!this._events[type]) {
      // Optimize the case of one listener. Don't need the extra array object.
      this._events[type] = listener;
    }
    else {
      if (typeof this._events[type] === 'function') {
        // Change to array.
        this._events[type] = [this._events[type]];
      }

      // If we've already got an array, just add
      if(prepend){
        this._events[type].unshift(listener);
      }else{
        this._events[type].push(listener);
      }

      // Check for listener leak
      if (
        !this._events[type].warned &&
        this._maxListeners > 0 &&
        this._events[type].length > this._maxListeners
      ) {
        this._events[type].warned = true;
        logPossibleMemoryLeak.call(this, this._events[type].length, type);
      }
    }

    return this;
  };

  EventEmitter.prototype.off = function(type, listener) {
    if (typeof listener !== 'function') {
      throw new Error('removeListener only takes instances of Function');
    }

    var handlers,leafs=[];

    if(this.wildcard) {
      var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
      leafs = searchListenerTree.call(this, null, ns, this.listenerTree, 0);
    }
    else {
      // does not use listeners(), so no side effect of creating _events[type]
      if (!this._events[type]) return this;
      handlers = this._events[type];
      leafs.push({_listeners:handlers});
    }

    for (var iLeaf=0; iLeaf<leafs.length; iLeaf++) {
      var leaf = leafs[iLeaf];
      handlers = leaf._listeners;
      if (isArray(handlers)) {

        var position = -1;

        for (var i = 0, length = handlers.length; i < length; i++) {
          if (handlers[i] === listener ||
            (handlers[i].listener && handlers[i].listener === listener) ||
            (handlers[i]._origin && handlers[i]._origin === listener)) {
            position = i;
            break;
          }
        }

        if (position < 0) {
          continue;
        }

        if(this.wildcard) {
          leaf._listeners.splice(position, 1);
        }
        else {
          this._events[type].splice(position, 1);
        }

        if (handlers.length === 0) {
          if(this.wildcard) {
            delete leaf._listeners;
          }
          else {
            delete this._events[type];
          }
        }
        if (this._removeListener)
          this.emit("removeListener", type, listener);

        return this;
      }
      else if (handlers === listener ||
        (handlers.listener && handlers.listener === listener) ||
        (handlers._origin && handlers._origin === listener)) {
        if(this.wildcard) {
          delete leaf._listeners;
        }
        else {
          delete this._events[type];
        }
        if (this._removeListener)
          this.emit("removeListener", type, listener);
      }
    }

    function recursivelyGarbageCollect(root) {
      if (root === undefined) {
        return;
      }
      var keys = Object.keys(root);
      for (var i in keys) {
        var key = keys[i];
        var obj = root[key];
        if ((obj instanceof Function) || (typeof obj !== "object") || (obj === null))
          continue;
        if (Object.keys(obj).length > 0) {
          recursivelyGarbageCollect(root[key]);
        }
        if (Object.keys(obj).length === 0) {
          delete root[key];
        }
      }
    }
    recursivelyGarbageCollect(this.listenerTree);

    return this;
  };

  EventEmitter.prototype.offAny = function(fn) {
    var i = 0, l = 0, fns;
    if (fn && this._all && this._all.length > 0) {
      fns = this._all;
      for(i = 0, l = fns.length; i < l; i++) {
        if(fn === fns[i]) {
          fns.splice(i, 1);
          if (this._removeListener)
            this.emit("removeListenerAny", fn);
          return this;
        }
      }
    } else {
      fns = this._all;
      if (this._removeListener) {
        for(i = 0, l = fns.length; i < l; i++)
          this.emit("removeListenerAny", fns[i]);
      }
      this._all = [];
    }
    return this;
  };

  EventEmitter.prototype.removeListener = EventEmitter.prototype.off;

  EventEmitter.prototype.removeAllListeners = function(type) {
    if (type === undefined) {
      !this._events || init.call(this);
      return this;
    }

    if (this.wildcard) {
      var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
      var leafs = searchListenerTree.call(this, null, ns, this.listenerTree, 0);

      for (var iLeaf=0; iLeaf<leafs.length; iLeaf++) {
        var leaf = leafs[iLeaf];
        leaf._listeners = null;
      }
    }
    else if (this._events) {
      this._events[type] = null;
    }
    return this;
  };

  EventEmitter.prototype.listeners = function(type) {
    if (this.wildcard) {
      var handlers = [];
      var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
      searchListenerTree.call(this, handlers, ns, this.listenerTree, 0);
      return handlers;
    }

    this._events || init.call(this);

    if (!this._events[type]) this._events[type] = [];
    if (!isArray(this._events[type])) {
      this._events[type] = [this._events[type]];
    }
    return this._events[type];
  };

  EventEmitter.prototype.eventNames = function(){
    return Object.keys(this._events);
  };

  EventEmitter.prototype.listenerCount = function(type) {
    return this.listeners(type).length;
  };

  EventEmitter.prototype.listenersAny = function() {

    if(this._all) {
      return this._all;
    }
    else {
      return [];
    }

  };

  if (typeof undefined === 'function' && undefined.amd) {
     // AMD. Register as an anonymous module.
    undefined(function() {
      return EventEmitter;
    });
  } else {
    // CommonJS
    module.exports = EventEmitter;
  }
}();
});

const eventizer = new eventemitter2();

/**
 * https://github.com/gre/bezier-easing
 * BezierEasing - use bezier curve for transition easing function
 * by Gaëtan Renaudeau 2014 - 2015 – MIT License
 */

// These values are established by empiricism with tests (tradeoff: performance VS precision)
var NEWTON_ITERATIONS = 4;
var NEWTON_MIN_SLOPE = 0.001;
var SUBDIVISION_PRECISION = 0.0000001;
var SUBDIVISION_MAX_ITERATIONS = 10;

var kSplineTableSize = 11;
var kSampleStepSize = 1.0 / (kSplineTableSize - 1.0);

var float32ArraySupported = typeof Float32Array === 'function';

function A (aA1, aA2) { return 1.0 - 3.0 * aA2 + 3.0 * aA1; }
function B (aA1, aA2) { return 3.0 * aA2 - 6.0 * aA1; }
function C (aA1)      { return 3.0 * aA1; }

// Returns x(t) given t, x1, and x2, or y(t) given t, y1, and y2.
function calcBezier (aT, aA1, aA2) { return ((A(aA1, aA2) * aT + B(aA1, aA2)) * aT + C(aA1)) * aT; }

// Returns dx/dt given t, x1, and x2, or dy/dt given t, y1, and y2.
function getSlope (aT, aA1, aA2) { return 3.0 * A(aA1, aA2) * aT * aT + 2.0 * B(aA1, aA2) * aT + C(aA1); }

function binarySubdivide (aX, aA, aB, mX1, mX2) {
  var currentX, currentT, i = 0;
  do {
    currentT = aA + (aB - aA) / 2.0;
    currentX = calcBezier(currentT, mX1, mX2) - aX;
    if (currentX > 0.0) {
      aB = currentT;
    } else {
      aA = currentT;
    }
  } while (Math.abs(currentX) > SUBDIVISION_PRECISION && ++i < SUBDIVISION_MAX_ITERATIONS);
  return currentT;
}

function newtonRaphsonIterate (aX, aGuessT, mX1, mX2) {
 for (var i = 0; i < NEWTON_ITERATIONS; ++i) {
   var currentSlope = getSlope(aGuessT, mX1, mX2);
   if (currentSlope === 0.0) {
     return aGuessT;
   }
   var currentX = calcBezier(aGuessT, mX1, mX2) - aX;
   aGuessT -= currentX / currentSlope;
 }
 return aGuessT;
}

var src = function bezier (mX1, mY1, mX2, mY2) {
  if (!(0 <= mX1 && mX1 <= 1 && 0 <= mX2 && mX2 <= 1)) {
    throw new Error('bezier x values must be in [0, 1] range');
  }

  // Precompute samples table
  var sampleValues = float32ArraySupported ? new Float32Array(kSplineTableSize) : new Array(kSplineTableSize);
  if (mX1 !== mY1 || mX2 !== mY2) {
    for (var i = 0; i < kSplineTableSize; ++i) {
      sampleValues[i] = calcBezier(i * kSampleStepSize, mX1, mX2);
    }
  }

  function getTForX (aX) {
    var intervalStart = 0.0;
    var currentSample = 1;
    var lastSample = kSplineTableSize - 1;

    for (; currentSample !== lastSample && sampleValues[currentSample] <= aX; ++currentSample) {
      intervalStart += kSampleStepSize;
    }
    --currentSample;

    // Interpolate to provide an initial guess for t
    var dist = (aX - sampleValues[currentSample]) / (sampleValues[currentSample + 1] - sampleValues[currentSample]);
    var guessForT = intervalStart + dist * kSampleStepSize;

    var initialSlope = getSlope(guessForT, mX1, mX2);
    if (initialSlope >= NEWTON_MIN_SLOPE) {
      return newtonRaphsonIterate(aX, guessForT, mX1, mX2);
    } else if (initialSlope === 0.0) {
      return guessForT;
    } else {
      return binarySubdivide(aX, intervalStart, intervalStart + kSampleStepSize, mX1, mX2);
    }
  }

  return function BezierEasing (x) {
    if (mX1 === mY1 && mX2 === mY2) {
      return x; // linear
    }
    // Because JavaScript number are imprecise, we should guarantee the extremes are right.
    if (x === 0) {
      return 0;
    }
    if (x === 1) {
      return 1;
    }
    return calcBezier(getTForX(x), mY1, mY2);
  };
};

const easeInFun = src(0.5, 0, 1, 1);
const easeOutFun = src(0, 0, 0.5, 1);
const easeInOutFun = src(0.5, 0, 0.5, 1);

function linear (x) {
  if (x > 1) {
    return 1;
  } else if (x < 0) {
    return 0;
  }

  return x;
}

function easeIn (x) {
  return easeInFun(x);
}

function easeOut (x) {
  return easeOutFun(x);
}

function easeInOut (x) {
  return easeInOutFun(x);
}


var timing = Object.freeze({
	linear: linear,
	easeIn: easeIn,
	easeOut: easeOut,
	easeInOut: easeInOut
});

let animations = [];
let working = false;

function workTick (time) {
  animations.forEach((animation, index) => {
    animation.tick.call(animation, time);
    if (animation.finished) {
      animations[index] = null;
    }
  });

  // cleanup all 'nulls'
  animations = animations.filter((animation) => {
    return animation;
  });

  if (animations.length > 0) {
    requestAnimationFrame(workTick);
  } else {
    working = false;
  }
}

function work () {
  if (working) {
    return;
  }

  working = true;

  if (animations.length === 0) {
    working = false;
    return;
  }

  requestAnimationFrame(workTick);
}

// animate(object, {
//   x: 100,
//   y: 200
// }, 1000).then(() => {
//   do things
// });

function animate (object, properties, duration = 1000, timingFun = 'linear') {
  const startTime = performance.now();
  const startProperties = {};
  const toDos = {};

  const animation = {
    duration,
    finished: false,
    object,
    properties,
    startTime,
    startProperties,
    timingFun,
    toDos
  };

  for (const property in properties) {
    startProperties[property] = object[property];
    toDos[property] = properties[property] - startProperties[property];
  }

  return new Promise((resolve) => {
    animation.resolve = resolve;
    // animation.tick will be called with `animation` context.
    animation.tick = function tick (time) {
      let timeFraction = (time - this.startTime) / this.duration;
      if (timeFraction > 1) {
        timeFraction = 1;
      }

      const progress = timing[this.timingFun](timeFraction);

      for (const property in this.properties) {
        this.object[property] = this.startProperties[property] + this.toDos[property] * progress;
      }

      // ENGINE SPECIFIC - DON'T USE THAT PROPERTY OUTSIDE THIS PROJECT
      // It is set in each iteration in case there are multiple animations running simultaneously
      if (typeof this.object.isAnimated !== 'undefined') {
        this.object.isAnimated = true;
      }
      // ./

      if (timeFraction < 1) {
        this.finished = false;
      } else {
        // ENGINE SPECIFIC
        if (typeof object.isAnimated !== 'undefined') {
          object.isAnimated = false;
        }
        // ./

        this.finished = true;
        this.resolve();
      }
    };

    animations.push(animation);
    work();
  });
}

function calculateMaxedSize (width, height, maxWidth, maxHeight) {
  const widthScale = maxWidth / width;
  const heightScale = maxHeight / height;
  const scale = (widthScale > heightScale) ? heightScale : widthScale;

  width *= scale;
  height *= scale;

  return [width, height, scale];
}

const config = {
  cardAnimationTime: 1000
};

var positionCards = function ({
  activeCard = -1,
  cardSize,
  parentSize,
  parentStart,
  direction,
  tight = false,
  secondDirectionPos,
  orientation,
  side,
  cards
}) {
  const secondDirection = direction === 'x' ? 'y' : 'x';
  const cardsCount = cards.length;
  const parentPadding = 20;
  let interval = 0;

  parentSize -= parentPadding * 2;
  // Don't make space for last card, it is always visible

  if (cardsCount === 0) {
    return;
  } else if (cardsCount === 1 || tight) {
    interval = 0;
  } else {
    interval = (parentSize - cardSize) / (cardsCount - 1);
  }

  // Limit interval
  const intervalLimit = 0.8 * cardSize;
  if (interval > intervalLimit) {
    interval = intervalLimit;
  }

  if (interval < 0) {
    interval = 0;
  }

  const additionalSpace = cardSize * 1.2 - interval;
  const afterActiveCard = cardsCount - activeCard - 1;

  const cardAnimationPromises = [];

  return new Promise((resolve) => {
    cards.forEach((card, index) => {
      card.side = side;
      card.orientation = orientation;

      const [width, height, scale] = calculateMaxedSize(
        card.zeroWidth,
        card.zeroHeight,
        direction === 'x' ? cardSize : 9999,
        direction === 'y' ? cardSize : 9999
      );

      let position = parentStart + parentPadding + cardSize / 2 + interval * index;

      if (activeCard > -1 && !tight) {
        if (index > activeCard) {
          position += additionalSpace;
        }

        if (index > activeCard) {
          let move = (additionalSpace / afterActiveCard) * (index - activeCard - 1);
          if (move > interval * (index - activeCard - 1)) {
            move = interval * (index - activeCard - 1);
          }

          position -= move;
        }
      }

      card.zIndex = index;

      const promise = animate(card, {
        [direction]: position,
        [secondDirection]: secondDirectionPos,
        scale
      }, config.cardAnimationTime, 'easeInOut');
      cardAnimationPromises.push(promise);
    });

    Promise.all(cardAnimationPromises).then(() => {
      resolve();
    });
  });
};

function BoardObject (player, place, state, {
  bare = false,
  cardSize,
  direction,
  height,
  orientation,
  side,
  tight = false,
  width,
  x,
  y
}) {
  this._bare = bare;
  this._cards = [];
  this._cardSize = cardSize;
  this._direction = direction;
  this._height = height;
  this._orientation = orientation;
  this._side = side;
  this._player = player;
  this._place = place;
  this._tight = tight;
  this._width = width;
  this._x = x;
  this._y = y;

  this._sortingLayer = new PIXI.display.Layer();
  this._sortingLayer.group.enableSort = true;
  state.stage.addChild(this._sortingLayer);
}

BoardObject.prototype = {
  addCard (card) {
    if (this._cards.indexOf(card) > -1) {
      return false;
    }

    card.parentLayer = this._sortingLayer;
    this._cards.push(card);
    return true;
  },
  get cards () { return this._cards; },
  get place () { return this._place; },
  get player () { return this._player; },
  positionCards (activeCard = -1) {
    return new Promise((resolve) => {
      positionCards({
        activeCard,
        cards: this._cards,
        cardSize: this._cardSize,
        direction: this._direction,
        orientation: this._orientation,
        parentSize: this._direction === 'x' ? this._width : this._height,
        parentStart: this._direction === 'x' ? this._x : this._y,
        secondDirectionPos: (this._direction === 'x' ? this._y + this._height / 2 : this._x + this._width / 2),
        side: this._side,
        tight: this._tight
      }).then(() => {
        this.hideUnneccessaryCards();
        resolve();
      });
    });
  },
  hideUnneccessaryCards () {
    // Hide only if it's bare BoardObject
    if (!this._bare) {
      return;
    }

    this._cards.forEach((card, index) => {
      // Left only last card visible
      if (index !== this._cards.length - 1) {
        card.visible = false;
      }
    });
  }
};

class TextObject extends PIXI.Text {
  constructor (x, y, text, style = {
    fontFamily: 'Arial',
    fontSize: 64,
    fill: '#fff'
  }) {
    super(text, style);
    this.x = x;
    this.y = y;
  }
}

class PowerInfo extends TextObject {
  constructor () {
    super(...arguments);
  }

  update (data) {
    let text = '';

    for (const type in data) {
      text += `${type}: `;
      text += `${data[type][0]} / ${data[type][1]}`;
      text += '\n';
    }

    this.text = text;
  }
}

var createStructure = function (state) {
  const structure = {
    player: {
      deck: new BoardObject('player', 'deck', state, {
        bare: true,
        cardSize: 500,
        direction: 'y',
        height: 450,
        orientation: false,
        side: false,
        tight: true,
        width: 600,
        x: 100,
        y: 1460
      }),
      hand: new BoardObject('player', 'hand', state, {
        cardSize: 300,
        direction: 'x',
        height: 300,
        orientation: true,
        side: true,
        width: 1910,
        x: 910,
        y: 1760
      }),
      graveyard: new BoardObject('player', 'graveyard', state, {
        bare: true,
        cardSize: 500,
        direction: 'y',
        height: 450,
        orientation: false,
        side: true,
        tight: true,
        width: 600,
        x: 3040,
        y: 1460
      }),
      powerInfo: new PowerInfo(500, 600, 'test')
    },
    enemy: {
      deck: new BoardObject('enemy', 'deck', state, {
        bare: true,
        cardSize: 500,
        direction: 'y',
        height: 450,
        orientation: false,
        side: false,
        tight: true,
        width: 600,
        x: 100,
        y: 50
      }),
      hand: new BoardObject('enemy', 'hand', state, {
        cardSize: 300,
        direction: 'x',
        height: 300,
        orientation: true,
        side: false,
        width: 1910,
        x: 910,
        y: 0
      }),
      graveyard: new BoardObject('enemy', 'graveyard', state, {
        bare: true,
        cardSize: 500,
        direction: 'y',
        height: 450,
        orientation: false,
        side: true,
        tight: true,
        width: 600,
        x: 3040,
        y: 50
      })
    }
  };

  state.stage.addChild(structure.player.powerInfo);

  return structure;
};

function resolveCard (card, objects) {
  for (const player in objects) {
    for (const place in objects[player]) {
      const object = objects[player][place];

      if (!(object instanceof BoardObject)) {
        continue;
      }

      const index = object.cards.indexOf(card);

      if (index > -1) {
        return {
          index,
          place: object.place,
          player: object.player
        };
      }
    }
  }

  return false;
}

function initialize (state) {
  this._objects = createStructure(state);

  eventizer.on('card.click', (card) => {
    const resolve = resolveCard(card, this._objects);
    if (resolve.place === 'hand') {
      this.manageActiveCard(card, resolve);
    }

    card.flip();

    this._objects.player.powerInfo.update({
      q: [2, 5],
      ch: [4, 4]
    });

    const activeCard = card.active ? resolve.index : -1;
    this._objects[resolve.player][resolve.place].positionCards(activeCard);
  });
}

function Board (state) {
  this._objects = null;
  this.activeCard = {
    card: null,
    resolve: null
  };

  const boardTexture = state.store.textures['board'];
  this.body = new PIXI.Sprite(boardTexture);
}

Board.prototype = {
  get cards () { return this._cards; },
  get view () { return this._view; },
  addCard (player, place, card) {
    if (['player'].includes(player) && ['hand'].includes(place)) {
      card.interaction = true;
    } else {
      card.interaction = false;
    }
    
    this._objects[player][place].addCard(card);
  },
  initialize (...args) { initialize.call(this, ...args); },
  manageActiveCard (card, resolve) {
    if (card.active) {
      card.active = false;
      this.activeCard.card = null;
      this.activeCard.resolve = null;
      return;
    }

    this._objects[resolve.player][resolve.place].cards.forEach((card) => {
      card.active = false;
    });

    card.active = true;

    this.activeCard.card = card;
    this.activeCard.resolve = resolve;
  },
  positionAllCards () {
    ['player', 'enemy'].forEach((player) => {
      ['deck', 'graveyard', 'hand'].forEach((place) => {
        this._objects[player][place].positionCards();
      });
    });
  }
};

/**
 * Returns a function, that, as long as it continues to be invoked, will not
 * be triggered. The function will be called after it stops being called for
 * N milliseconds. If `immediate` is passed, trigger the function on the
 * leading edge, instead of the trailing. The function also has a property 'clear' 
 * that is a function which will clear the timer to prevent previously scheduled executions. 
 *
 * @source underscore.js
 * @see http://unscriptable.com/2009/03/20/debouncing-javascript-methods/
 * @param {Function} function to wrap
 * @param {Number} timeout in ms (`100`)
 * @param {Boolean} whether to execute at the beginning (`false`)
 * @api public
 */

var debounce = function debounce(func, wait, immediate){
  var timeout, args, context, timestamp, result;
  if (null == wait) wait = 100;

  function later() {
    var last = Date.now() - timestamp;

    if (last < wait && last >= 0) {
      timeout = setTimeout(later, wait - last);
    } else {
      timeout = null;
      if (!immediate) {
        result = func.apply(context, args);
        context = args = null;
      }
    }
  }

  var debounced = function(){
    context = this;
    args = arguments;
    timestamp = Date.now();
    var callNow = immediate && !timeout;
    if (!timeout) timeout = setTimeout(later, wait);
    if (callNow) {
      result = func.apply(context, args);
      context = args = null;
    }

    return result;
  };

  debounced.clear = function() {
    if (timeout) {
      clearTimeout(timeout);
      timeout = null;
    }
  };
  
  debounced.flush = function() {
    if (timeout) {
      result = func.apply(context, args);
      context = args = null;
      
      clearTimeout(timeout);
      timeout = null;
    }
  };

  return debounced;
};

class Card extends PIXI.Sprite {
  constructor (state, {
    name,
    x,
    y
  }) {
    super(state.store.textures['card/reverse']);

    this._orientation = true;
    this._side = false;
    this._scaleX = 1;
    this._scaleY = 1;
    this._scale = 1;
    this._state = state;

    this.active = false;
    this.isAnimated = false;
    this.name = name;
    this.zeroWidth = 720;
    this.zeroHeight = 1000;

    this.anchor.set(0.5);
    this.x = x;
    this.y = y;

    this.initialize();
  }

  flip (side = null) {
    this.isAnimated = true;
    const duration = config.cardAnimationTime / 2;

    if (side === this._side) {
      return new Promise((resolve) => {
        resolve();
      });
    }

    return new Promise((resolve) => {
      if (this._side) {
        animate(this, { scaleX: 0 }, duration, 'easeIn')
          .then(() => {
            this.swapTexture(false);
            return animate(this, { scaleX: 1 }, duration, 'easeOut');
          })
          .then(() => {
            this._side = false;
            this.isAnimated = false;

            resolve();
          });
      } else {
        animate(this, { scaleX: 0 }, duration, 'easeIn')
          .then(() => {
            this.swapTexture(true);
            return animate(this, { scaleX: 1 }, duration, 'easeOut');
          })
          .then(() => {
            this._side = true;
            this.isAnimated = false;

            resolve();
          });
      }
    });
  }

  initialize () {
    this.on('pointertap', () => {
      if (this.isAnimated) {
        return;
      }

      this.onClick();
    });
  }

  onClick () {
    eventizer.emit('card.click', this);
  }

  swapTexture (side) {
    if (side) {
      this.texture = this._state.store.textures[this.name];
    } else {
      this.texture = this._state.store.textures['card/reverse'];
    }
  }

  get height () {
    if (!this._orientation) {
      return super.width;
    }

    return super.height;
  }

  set height (value) { return; }

  set interaction (value) {
    this.interactive = value;
    this.buttonMode = value;
  }

  get orientation () { return this._orientation; }

  set orientation (value) {
    this._orientation = value;

    animate(this, {
      rotation: this._orientation ? 0 : Math.PI / 2
    }, 300);
  }

  get scaleX () { return super.scale.x; }

  set scaleX (value) {
    this._scaleX = value;
    super.scale.x = this._scale * this._scaleX;
  }

  get scaleY () { return super.scale.y; }

  set scaleY (value) {
    this._scaleY = value;
    super.scale.y = this._scale * this._scaleY;
  }

  get scale () { return this._scale; }

  set scale (value) {
    this._scale = value;
    super.scale.x = this._scale * this._scaleX;
    super.scale.y = this._scale * this._scaleY;
  }

  get side () { return this._side; }

  set side (value) {
    if (value !== this._side) {
      this.flip();
      this._side = value;
    }
  }

  get width () {
    if (!this._orientation) {
      return super.height;
    }

    return super.width;
  }

  set width (value) { return; }
}

var gameLoop = function (delta) {
};

const store = {};

var textures = [{"name":"board","url":"assets/images/board.jpg"},{"name":"card/reverse","url":"assets/images/card/reverse.jpg"},{"name":"card/da/3","url":"assets/images/cards/da/3.jpg"}];
var texturepacks = [];
var resources = {
	textures: textures,
	texturepacks: texturepacks
};

const pixiConfiguration = {
  width: 64,
  height: 64,
  antialias: true
};

const app = new PIXI.Application(pixiConfiguration);
app.stage = new PIXI.display.Stage();
app.stage.group.enableSort = true;
app.renderer.view.style.position = 'absolute';
app.renderer.autoResize = true;
const state = {
  stage: app.stage,
  store
};

function resizePixi () {
  // Dimensions are board.jpg size. Everything should be scaled according to that size.
  const [width, height, scale] = calculateMaxedSize(
    3740,
    2060,
    window.innerWidth,
    window.innerHeight
  );

  app.renderer.resize(width, height);
  app.stage.scale.set(scale);
}

function setup () {
  window.addEventListener('resize', debounce(() => {
    resizePixi();
  }), 500);
  resizePixi();

  const board = new Board(state);
  app.stage.addChild(board.body);
  board.initialize(state);

  ['player', 'enemy'].forEach((player) => {
    ['deck', 'graveyard', 'hand'].forEach((place) => {
      for (let i = 0; i < 15; i += 1) {
        const card = new Card(state, {
          name: 'card/da/3',
          x: Math.random() * 1000,
          y: Math.random() * 1000
        });
        app.stage.addChild(card);
        board.addCard(player, place, card);
      }
    });
  });

  board.positionAllCards();

  app.ticker.add((delta) => {
    gameLoop.call(state, delta);
  });
}

load(store, resources).then(setup);

const view = app.view;

document.body.appendChild(view);

})));
//# sourceMappingURL=bundle.js.map
