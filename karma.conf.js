const path = require('path');

module.exports = (config) => {
  config.set({
    plugins: [
      'karma-jasmine',
      'karma-jasmine-matchers',
      'karma-mocha-reporter',
      'karma-chrome-launcher',
      'karma-rollup-preprocessor'
    ],

    basePath: '',

    browserConsoleLogOptions: {
      level: 'error'
    },

    frameworks: [
      'jasmine',
      'jasmine-matchers'
    ],

    files: [
      'tests/env/pixi.min.js',
      'tests/env/pixi-layers.js',
      'tests/env/env.js',
      { pattern: 'tests/**/*.test.js', watched: false }
    ],

    exclude: [],

    preprocessors: {
      'tests/**/*.test.js': ['rollupNode']
    },

    rollupPreprocessor: {
      plugins: [require('rollup-plugin-buble')()],
      output: {
        format: 'iife',
        name: 'test',
        sourcemap: 'inline'
      }
    },

    customPreprocessors: {
      rollupNode: {
        base: 'rollup',
        options: {
          plugins: [
            require('rollup-plugin-alias')({
              '@': path.join(__dirname, './src'),
              '%': path.join(__dirname, './tests'),
              'resolve': ['.js', '.json']
            }),
            require('rollup-plugin-node-resolve')({
              preferBuiltins: true
            }),
            require('rollup-plugin-commonjs')(),
            require('rollup-plugin-json')(),
            require('rollup-plugin-buble')()
          ]
        }
      }
    },

    reporters: ['mocha'],

    port: 9876,

    colors: true,

    logLevel: config.LOG_INFO,

    autoWatch: true,

    browsers: ['ChromiumNoSandbox'],
    customLaunchers: {
      ChromiumNoSandbox: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox']
      }
    },

    singleRun: true,

    concurrency: Infinity
  });
};
