import * as timing from '@/timing';

let animations = [];
let working = false;

function workTick (time) {
  animations.forEach((animation, index) => {
    animation.tick.call(animation, time);
    if (animation.finished) {
      animations[index] = null;
    }
  });

  // cleanup all 'nulls'
  animations = animations.filter((animation) => {
    return animation;
  });

  if (animations.length > 0) {
    requestAnimationFrame(workTick);
  } else {
    working = false;
  }
}

function work () {
  if (working) {
    return;
  }

  working = true;

  if (animations.length === 0) {
    working = false;
    return;
  }

  requestAnimationFrame(workTick);
}

// animate(object, {
//   x: 100,
//   y: 200
// }, 1000).then(() => {
//   do things
// });

function animate (object, properties, duration = 1000, timingFun = 'linear') {
  const startTime = performance.now();
  const startProperties = {};
  const toDos = {};

  const animation = {
    duration,
    finished: false,
    object,
    properties,
    startTime,
    startProperties,
    timingFun,
    toDos
  };

  for (const property in properties) {
    startProperties[property] = object[property];
    toDos[property] = properties[property] - startProperties[property];
  }

  return new Promise((resolve) => {
    animation.resolve = resolve;
    // animation.tick will be called with `animation` context.
    animation.tick = function tick (time) {
      let timeFraction = (time - this.startTime) / this.duration;
      if (timeFraction > 1) {
        timeFraction = 1;
      }

      const progress = timing[this.timingFun](timeFraction);

      for (const property in this.properties) {
        this.object[property] = this.startProperties[property] + this.toDos[property] * progress;
      }

      // ENGINE SPECIFIC - DON'T USE THAT PROPERTY OUTSIDE THIS PROJECT
      // It is set in each iteration in case there are multiple animations running simultaneously
      if (typeof this.object.isAnimated !== 'undefined') {
        this.object.isAnimated = true;
      }
      // ./

      if (timeFraction < 1) {
        this.finished = false;
      } else {
        // ENGINE SPECIFIC
        if (typeof object.isAnimated !== 'undefined') {
          object.isAnimated = false;
        }
        // ./

        this.finished = true;
        this.resolve();
      }
    };

    animations.push(animation);
    work();
  });
}

export default animate;