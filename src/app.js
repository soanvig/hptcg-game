import * as loader from './loader';
import Board from './objects/Board';
import Card from './objects/Card';

import { calculateMaxedSize, debounce } from './helpers';
import gameLoop from './gameLoop';
import store from './store';
import resources from './resources.json';

const pixiConfiguration = {
  width: 64,
  height: 64,
  antialias: true
};

const app = new PIXI.Application(pixiConfiguration);
app.stage = new PIXI.display.Stage();
app.stage.group.enableSort = true;
app.renderer.view.style.position = 'absolute';
app.renderer.autoResize = true;
const state = {
  stage: app.stage,
  store
};

function resizePixi () {
  // Dimensions are board.jpg size. Everything should be scaled according to that size.
  const [width, height, scale] = calculateMaxedSize(
    3740,
    2060,
    window.innerWidth,
    window.innerHeight
  );

  app.renderer.resize(width, height);
  app.stage.scale.set(scale);
}

function setup () {
  window.addEventListener('resize', debounce(() => {
    resizePixi();
  }), 500);
  resizePixi();

  const board = new Board(state);
  app.stage.addChild(board.body);
  board.initialize(state);

  ['player', 'enemy'].forEach((player) => {
    ['deck', 'graveyard', 'hand'].forEach((place) => {
      for (let i = 0; i < 15; i += 1) {
        const card = new Card(state, {
          name: 'card/da/3',
          x: Math.random() * 1000,
          y: Math.random() * 1000
        });
        app.stage.addChild(card);
        board.addCard(player, place, card);
      }
    });
  });

  board.positionAllCards();

  app.ticker.add((delta) => {
    gameLoop.call(state, delta);
  });
}

loader.load(store, resources).then(setup);

const view = app.view;
export {
  view
};