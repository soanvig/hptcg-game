const config = {
  cardAnimationTime: window.env === 'test' ? 10 : 1000
};

export default config;

