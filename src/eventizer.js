import EventEmitter2 from 'eventemitter2';

const eventizer = new EventEmitter2();

export default eventizer;