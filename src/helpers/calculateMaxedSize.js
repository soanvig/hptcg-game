export default function calculateMaxedSize (width, height, maxWidth, maxHeight) {
  const widthScale = maxWidth / width;
  const heightScale = maxHeight / height;
  const scale = (widthScale > heightScale) ? heightScale : widthScale;

  width *= scale;
  height *= scale;

  return [width, height, scale];
}