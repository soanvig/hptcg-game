function install (store, resources) {
  store.textures = {};

  for (const name in resources) {
    store.textures[name] = PIXI.Texture.from(resources[name].url);
  }
}

function loaderProgress (loader, resource) {
  console.log(`Loading: "${resource.name}"`);
  console.log(`Progress: ${loader.progress}%`);
}

function load (store, resources) {
  let toLoad = [];
  for (const resource in resources) {
    toLoad = toLoad.concat(resources[resource]);
  }

  const loader = new PIXI.loaders.Loader();
  loader.onProgress.add(loaderProgress);
  loader.add(toLoad);

  return new Promise((resolve) => {
    loader.load((loader, resources) => {
      install(store, resources);
      resolve();
    });
  });
}

export {
  install,
  loaderProgress,
  load
};