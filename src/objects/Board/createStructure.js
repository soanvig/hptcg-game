import BoardObject from '@/objects/BoardObject';
import PowerInfo from '@/objects/PowerInfo';

export default function (state) {
  const structure = {
    player: {
      deck: new BoardObject('player', 'deck', state, {
        bare: true,
        cardSize: 500,
        direction: 'y',
        height: 450,
        orientation: false,
        side: false,
        tight: true,
        width: 600,
        x: 100,
        y: 1460
      }),
      hand: new BoardObject('player', 'hand', state, {
        cardSize: 300,
        direction: 'x',
        height: 300,
        orientation: true,
        side: true,
        width: 1910,
        x: 910,
        y: 1760
      }),
      graveyard: new BoardObject('player', 'graveyard', state, {
        bare: true,
        cardSize: 500,
        direction: 'y',
        height: 450,
        orientation: false,
        side: true,
        tight: true,
        width: 600,
        x: 3040,
        y: 1460
      }),
      powerInfo: new PowerInfo(500, 600, 'test')
    },
    enemy: {
      deck: new BoardObject('enemy', 'deck', state, {
        bare: true,
        cardSize: 500,
        direction: 'y',
        height: 450,
        orientation: false,
        side: false,
        tight: true,
        width: 600,
        x: 100,
        y: 50
      }),
      hand: new BoardObject('enemy', 'hand', state, {
        cardSize: 300,
        direction: 'x',
        height: 300,
        orientation: true,
        side: false,
        width: 1910,
        x: 910,
        y: 0
      }),
      graveyard: new BoardObject('enemy', 'graveyard', state, {
        bare: true,
        cardSize: 500,
        direction: 'y',
        height: 450,
        orientation: false,
        side: true,
        tight: true,
        width: 600,
        x: 3040,
        y: 50
      })
    }
  };

  state.stage.addChild(structure.player.powerInfo);

  return structure;
}