import eventizer from '@/eventizer';
import createStructure from './createStructure';
import resolveCard from './resolveCard';

function initialize (state) {
  this._objects = createStructure(state);

  eventizer.on('card.click', (card) => {
    const resolve = resolveCard(card, this._objects);
    if (resolve.place === 'hand') {
      this.manageActiveCard(card, resolve);
    }

    card.flip();

    this._objects.player.powerInfo.update({
      q: [2, 5],
      ch: [4, 4]
    });

    const activeCard = card.active ? resolve.index : -1;
    this._objects[resolve.player][resolve.place].positionCards(activeCard);
  });
}

function Board (state) {
  this._objects = null;
  this.activeCard = {
    card: null,
    resolve: null
  };

  const boardTexture = state.store.textures['board'];
  this.body = new PIXI.Sprite(boardTexture);
}

Board.prototype = {
  get cards () { return this._cards; },
  get view () { return this._view; },
  addCard (player, place, card) {
    if (['player'].includes(player) && ['hand'].includes(place)) {
      card.interaction = true;
    } else {
      card.interaction = false;
    }
    
    this._objects[player][place].addCard(card);
  },
  initialize (...args) { initialize.call(this, ...args); },
  manageActiveCard (card, resolve) {
    if (card.active) {
      card.active = false;
      this.activeCard.card = null;
      this.activeCard.resolve = null;
      return;
    }

    this._objects[resolve.player][resolve.place].cards.forEach((card) => {
      card.active = false;
    });

    card.active = true;

    this.activeCard.card = card;
    this.activeCard.resolve = resolve;
  },
  positionAllCards () {
    ['player', 'enemy'].forEach((player) => {
      ['deck', 'graveyard', 'hand'].forEach((place) => {
        this._objects[player][place].positionCards();
      });
    });
  }
};

export default Board;