import BoardObject from '@/objects/BoardObject';

export default function resolveCard (card, objects) {
  for (const player in objects) {
    for (const place in objects[player]) {
      const object = objects[player][place];

      if (!(object instanceof BoardObject)) {
        continue;
      }

      const index = object.cards.indexOf(card);

      if (index > -1) {
        return {
          index,
          place: object.place,
          player: object.player
        };
      }
    }
  }

  return false;
}