import positionCards from './positionCards';

function BoardObject (player, place, state, {
  bare = false,
  cardSize,
  direction,
  height,
  orientation,
  side,
  tight = false,
  width,
  x,
  y
}) {
  this._bare = bare;
  this._cards = [];
  this._cardSize = cardSize;
  this._direction = direction;
  this._height = height;
  this._orientation = orientation;
  this._side = side;
  this._player = player;
  this._place = place;
  this._tight = tight;
  this._width = width;
  this._x = x;
  this._y = y;

  this._sortingLayer = new PIXI.display.Layer();
  this._sortingLayer.group.enableSort = true;
  state.stage.addChild(this._sortingLayer);
}

BoardObject.prototype = {
  addCard (card) {
    if (this._cards.indexOf(card) > -1) {
      return false;
    }

    card.parentLayer = this._sortingLayer;
    this._cards.push(card);
    return true;
  },
  get cards () { return this._cards; },
  get place () { return this._place; },
  get player () { return this._player; },
  positionCards (activeCard = -1) {
    return new Promise((resolve) => {
      positionCards({
        activeCard,
        cards: this._cards,
        cardSize: this._cardSize,
        direction: this._direction,
        orientation: this._orientation,
        parentSize: this._direction === 'x' ? this._width : this._height,
        parentStart: this._direction === 'x' ? this._x : this._y,
        secondDirectionPos: (this._direction === 'x' ? this._y + this._height / 2 : this._x + this._width / 2),
        side: this._side,
        tight: this._tight
      }).then(() => {
        this.hideUnneccessaryCards();
        resolve();
      });
    });
  },
  hideUnneccessaryCards () {
    // Hide only if it's bare BoardObject
    if (!this._bare) {
      return;
    }

    this._cards.forEach((card, index) => {
      // Left only last card visible
      if (index !== this._cards.length - 1) {
        card.visible = false;
      }
    });
  }
};

export default BoardObject;