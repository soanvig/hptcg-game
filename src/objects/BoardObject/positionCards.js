import animate from '@/animate';
import calculateMaxedSize from '@/helpers/calculateMaxedSize';
import config from '@/config';

export default function ({
  activeCard = -1,
  cardSize,
  parentSize,
  parentStart,
  direction,
  tight = false,
  secondDirectionPos,
  orientation,
  side,
  cards
}) {
  const secondDirection = direction === 'x' ? 'y' : 'x';
  const cardsCount = cards.length;
  const parentPadding = 20;
  let interval = 0;

  parentSize -= parentPadding * 2;
  // Don't make space for last card, it is always visible

  if (cardsCount === 0) {
    return;
  } else if (cardsCount === 1 || tight) {
    interval = 0;
  } else {
    interval = (parentSize - cardSize) / (cardsCount - 1);
  }

  // Limit interval
  const intervalLimit = 0.8 * cardSize;
  if (interval > intervalLimit) {
    interval = intervalLimit;
  }

  if (interval < 0) {
    interval = 0;
  }

  const additionalSpace = cardSize * 1.2 - interval;
  const afterActiveCard = cardsCount - activeCard - 1;

  const cardAnimationPromises = [];

  return new Promise((resolve) => {
    cards.forEach((card, index) => {
      card.side = side;
      card.orientation = orientation;

      const [width, height, scale] = calculateMaxedSize(
        card.zeroWidth,
        card.zeroHeight,
        direction === 'x' ? cardSize : 9999,
        direction === 'y' ? cardSize : 9999
      );

      let position = parentStart + parentPadding + cardSize / 2 + interval * index;

      if (activeCard > -1 && !tight) {
        if (index > activeCard) {
          position += additionalSpace;
        }

        if (index > activeCard) {
          let move = (additionalSpace / afterActiveCard) * (index - activeCard - 1);
          if (move > interval * (index - activeCard - 1)) {
            move = interval * (index - activeCard - 1);
          }

          position -= move;
        }
      }

      card.zIndex = index;

      const promise = animate(card, {
        [direction]: position,
        [secondDirection]: secondDirectionPos,
        scale
      }, config.cardAnimationTime, 'easeInOut');
      cardAnimationPromises.push(promise);
    });

    Promise.all(cardAnimationPromises).then(() => {
      resolve();
    });
  });
}