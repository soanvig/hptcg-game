import animate from '@/animate';
import config from '@/config';
import eventizer from '@/eventizer';

class Card extends PIXI.Sprite {
  constructor (state, {
    name,
    x,
    y
  }) {
    super(state.store.textures['card/reverse']);

    this._orientation = true;
    this._side = false;
    this._scaleX = 1;
    this._scaleY = 1;
    this._scale = 1;
    this._state = state;

    this.active = false;
    this.isAnimated = false;
    this.name = name;
    this.zeroWidth = 720;
    this.zeroHeight = 1000;

    this.anchor.set(0.5);
    this.x = x;
    this.y = y;

    this.initialize();
  }

  flip (side = null) {
    this.isAnimated = true;
    const duration = config.cardAnimationTime / 2;

    if (side === this._side) {
      return new Promise((resolve) => {
        this.isAnimated = false;
        resolve();
      });
    }

    return new Promise((resolve) => {
      if (this._side) {
        animate(this, { scaleX: 0 }, duration, 'easeIn')
          .then(() => {
            this.swapTexture(false);
            return animate(this, { scaleX: 1 }, duration, 'easeOut');
          })
          .then(() => {
            this._side = false;
            this.isAnimated = false;

            resolve();
          });
      } else {
        animate(this, { scaleX: 0 }, duration, 'easeIn')
          .then(() => {
            this.swapTexture(true);
            return animate(this, { scaleX: 1 }, duration, 'easeOut');
          })
          .then(() => {
            this._side = true;
            this.isAnimated = false;

            resolve();
          });
      }
    });
  }

  initialize () {
    this.on('pointertap', () => {
      if (this.isAnimated) {
        return;
      }

      this.onClick();
    });
  }

  onClick () {
    eventizer.emit('card.click', this);
  }

  swapTexture (side) {
    if (side) {
      this.texture = this._state.store.textures[this.name];
    } else {
      this.texture = this._state.store.textures['card/reverse'];
    }
  }

  get height () {
    if (!this._orientation) {
      return super.width;
    }

    return super.height;
  }

  set height (value) { return; }

  set interaction (value) {
    this.interactive = value;
    this.buttonMode = value;
  }

  get orientation () { return this._orientation; }

  set orientation (value) {
    this._orientation = value;

    animate(this, {
      rotation: this._orientation ? 0 : Math.PI / 2
    }, 300);
  }

  get scaleX () { return this.scale.x; }

  set scaleX (value) {
    this._scaleX = value;
    this.scale.x = this._scale * this._scaleX;
  }

  get scaleY () { return this.scale.y; }

  set scaleY (value) {
    this._scaleY = value;
    this.scale.y = this._scale * this._scaleY;
  }

  get scaling () { return this._scale; }

  set scaling (value) {
    this._scale = value;
    this.scale.x = this._scale * this._scaleX;
    this.scale.y = this._scale * this._scaleY;
  }

  get side () { return this._side; }

  set side (value) {
    if (value !== this._side) {
      this.flip();
      this._side = value;
    }
  }

  get width () {
    if (!this._orientation) {
      return super.height;
    }

    return super.width;
  }

  set width (value) { return; }
}

export default Card;