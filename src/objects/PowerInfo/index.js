import TextObject from '@/objects/TextObject';

class PowerInfo extends TextObject {
  constructor () {
    super(...arguments);
  }

  update (data) {
    let text = '';

    for (const type in data) {
      text += `${type}: `;
      text += `${data[type][0]} / ${data[type][1]}`;
      text += '\n';
    }

    this.text = text;
  }
}

export default PowerInfo;