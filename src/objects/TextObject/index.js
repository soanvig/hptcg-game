class TextObject extends PIXI.Text {
  constructor (x, y, text, style = {
    fontFamily: 'Arial',
    fontSize: 64,
    fill: '#fff'
  }) {
    super(text, style);
    this.x = x;
    this.y = y;
  }
}

export default TextObject;