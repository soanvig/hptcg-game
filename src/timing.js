import Bezier from 'bezier-easing';

const easeInFun = Bezier(0.5, 0, 1, 1);
const easeOutFun = Bezier(0, 0, 0.5, 1);
const easeInOutFun = Bezier(0.5, 0, 0.5, 1);

export function linear (x) {
  if (x > 1) {
    return 1;
  } else if (x < 0) {
    return 0;
  }

  return x;
}

export function easeIn (x) {
  return easeInFun(x);
}

export function easeOut (x) {
  return easeOutFun(x);
}

export function easeInOut (x) {
  return easeInOutFun(x);
}
