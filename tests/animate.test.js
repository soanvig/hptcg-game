import animate from '@/animate';

describe('animate', () => {
  it('should animate properties', (done) => {
    const object = { foo: 0 };
    animate(object, { foo: 100 }).then(() => {
      expect(object.foo).toBe(100);
      done();
    });
  });
});