window.any = jasmine.any;
window.anything = jasmine.anything;
window.objectWith = jasmine.objectContaining;
window.env = 'test';
global = window;

const pixiConfiguration = {
  width: 1000,
  height: 1000,
  antialias: true
};

const app = new PIXI.Application(pixiConfiguration);
app.stage = new PIXI.display.Stage();

document.body.appendChild(app.view);

window.stage = app.stage;