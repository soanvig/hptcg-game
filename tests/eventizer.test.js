import EventEmitter2 from 'eventemitter2';
import eventizer from '@/eventizer';

describe('eventizer', () => {
  it('should be instance of EventEmmiter', () => {
    expect(eventizer).toEqual(any(EventEmitter2));
  });
});