import * as helpers from '@/helpers';

describe('calculateMaxedSize yields proper results', () => {
  describe('when maxWidth > width && maxHeight > height', () => {
    it('should yield proper width, height and scale', () => {
      const width = 1000;
      const height = 500;
      const maxWidth = 2000;
      const maxHeight = 750;

      const [resultWidth, resultHeight, resultScale] = helpers.calculateMaxedSize(width, height, maxWidth, maxHeight);

      expect(resultWidth).toBe(1500);
      expect(resultHeight).toBe(750);
      expect(resultScale).toBe(1.5);
    });
  });

  describe('when maxWidth < width && maxHeight < height', () => {
    it('should yield proper width, height and scale', () => {
      const width = 1000;
      const height = 500;
      const maxWidth = 500;
      const maxHeight = 250;

      const [resultWidth, resultHeight, resultScale] = helpers.calculateMaxedSize(width, height, maxWidth, maxHeight);

      expect(resultWidth).toBe(500);
      expect(resultHeight).toBe(250);
      expect(resultScale).toBe(0.5);
    });
  });

  describe('when maxWidth > width && maxHeight < height', () => {
    it('should yield proper width, height and scale', () => {
      const width = 1000;
      const height = 500;
      const maxWidth = 2000;
      const maxHeight = 250;

      const [resultWidth, resultHeight, resultScale] = helpers.calculateMaxedSize(width, height, maxWidth, maxHeight);

      expect(resultWidth).toBe(500);
      expect(resultHeight).toBe(250);
      expect(resultScale).toBe(0.5);
    });
  });
});

describe('debounce', () => {
  it('should be function', () => {
    expect(helpers.debounce).toBeFunction();
  });
});

describe('shuffle', () => {
  let array;
  let shuffled;

  beforeEach(() => {
    array = [0, 1, 2, 3, 4, 5];
    shuffled = helpers.shuffle(array);
  });

  it('should preserve length of original array', () => {
    expect(shuffled.length).toBe(array.length);
  });

  it('should return an array with at least two elements with changed positions', () => {
    let notInPosition = 0;

    shuffled.forEach((item, index) => {
      if (item !== array[index]) {
        notInPosition += 1;
      }
    });

    expect(notInPosition).toBeGreaterThan(0);
  });
});