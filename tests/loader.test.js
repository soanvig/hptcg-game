import * as loader from '@/loader';

import resources, { dummy } from '%/resources';

describe('install', () => {
  let store = {};
  const dummyResources = {
    foobar: {
      name: 'foobar',
      url: dummy
    }
  };

  beforeEach(() => {
    store = {};
  });

  it('should be function', () => {
    expect(loader.install).toBeFunction();
  });

  it('should create store.textures empty object', () => {
    loader.install(store, {});
    expect(store.textures).toEqual(objectWith({}));
  });

  it('should create store.textures[textureName] object', () => {
    loader.install(store, dummyResources);
    expect(store.textures.foobar).toBeDefined();
  });

  it('should create PIXI.Texture in store.textures[textureName] object', () => {
    loader.install(store, dummyResources);
    expect(store.textures.foobar).toEqual(any(PIXI.Texture));
  });
});

describe('loaderProgress', () => {
  it('should be function', () => {
    expect(loader.loaderProgress).toBeFunction();
  });
});

describe('load', () => {
  let store = {};

  beforeEach(() => {
    store = {};
  });

  it('should be function', () => {
    expect(loader.load).toBeFunction();
  });

  it('should return Promise and resolve', (done) => {
    loader.load(store, resources).then(() => {
      done();
    });
  });
});