import Card from '@/objects/Card';
import BoardObject from '@/objects/BoardObject';
import positionCards from '@/objects/BoardObject/positionCards.js';

import { load } from '@/loader';
import resources from '%/resources';

describe('BoardObject', () => {
  it('should be function', () => {
    expect(BoardObject).toBeFunction();
  });
});

describe('positionCards', () => {
  const state = {
    store: {}
  };

  const testConfig = {
    cards: null,
    cardSize: 100,
    direction: 'x',
    orientation: true,
    parentSize: 300,
    parentStart: 0,
    secondDirectionPos: 300,
    side: false,
    tight: false
  };

  beforeEach((done) => {
    load(state.store, resources).then(() => {
      const cards = [
        new Card(state, { name: 'foobar', x: 0, y: 0 }),
        new Card(state, { name: 'foobar', x: 100, y: 100 }),
        new Card(state, { name: 'foobar', x: 200, y: 200 }),
      ];

      done();
    });
  });

  it('should be function', () => {
    expect(positionCards).toBeFunction();
  });
});