import * as loader from '@/loader';

import resources from '%/resources';
import Card from '@/objects/Card';

describe('Card', () => {
  const state = { store: {} };
  const dummyProps = { name: 'foobar', x: 100, y: 50 };
  let card;

  function createNewCard () {
    if (card) {
      stage.removeChild(card);
    }

    card = new Card(state, dummyProps);
    stage.addChild(card);
  }

  beforeAll((done) => {
    loader.load(state.store, resources).then(() => {
      createNewCard();
      done();
    });
  });

  it('should be function', () => {
    expect(Card).toBeFunction();
  });

  it('should be child of PIXI.Sprite', () => {
    const parent = Object.getPrototypeOf(card.constructor);

    expect(parent).toBe(PIXI.Sprite);
  });

  describe('default properties', () => {
    it('should orientation be true', () => {
      expect(card._orientation).toBe(true);
    });

    it('should side be false', () => {
      expect(card._side).toBe(false);
    });

    it('should scale be 1', () => {
      expect(card._scale).toBe(1);
      expect(card._scaleX).toBe(1);
      expect(card._scaleY).toBe(1);
    });

    it('should active be false', () => {
      expect(card.active).toBe(false);
    });

    it('should not be animated', () => {
      expect(card.isAnimated).toBe(false);
    });

    it('should have zeroWidth = 720', () => {
      expect(card.zeroWidth).toBe(720);
    });

    it('should have zeroHeight = 1000', () => {
      expect(card.zeroHeight).toBe(1000);
    });

    it('should anchors in the middle', () => {
      expect(card.anchor.x).toBe(0.5);
      expect(card.anchor.y).toBe(0.5);
    });

    it('should be in position defined in constructor', () => {
      expect(card.x).toBe(100);
      expect(card.y).toBe(50);
    });
  });

  describe('initialize', () => {
    it('should be function', () => {
      expect(Card.prototype.initialize).toBeFunction();
    });

    it('should be called once during constructing', () => {
      spyOn(Card.prototype, 'initialize');

      createNewCard();

      expect(Card.prototype.initialize).toHaveBeenCalled();
    });
  });

  describe('flip', () => {
    beforeEach(() => {
      createNewCard();
    });

    it('should be function', () => {
      expect(Card.prototype.flip).toBeFunction();
    });

    it('should set isAnimated to true', () => {
      card.flip();
      expect(card.isAnimated).toBe(true);
    });

    describe('when no argument', () => {
      it('should return Promise and resolve', (done) => {
        card.flip().then(() => {
          done();
        });
      });

      it('should switch side to opposite', (done) => {
        const side = card._side;
        card.flip().then(() => {
          expect(card._side).not.toBe(side);
          done();
        });
      });

      describe('when side is false', () => {
        it('should set isAnimated to false after animation', (done) => {
          card._side = false;
          card.flip().then(() => {
            expect(card.isAnimated).toBe(false);
            done();
          });
        });
      });

      describe('when side is true', () => {
        it('should set isAnimated to true after animation', (done) => {
          card._side = true;
          card.flip().then(() => {
            expect(card.isAnimated).toBe(false);
            done();
          });
        });
      });
    });

    describe('when argument is provided', () => {
      describe('when side is equal to card side', () => {
        it('should not flip side', (done) => {
          card._side = false;
          card.flip(false).then(() => {
            expect(card._side).not.toBe(true);
            done();
          });
        });

        it('should set isAnimated to false', (done) => {
          card._side = false;
          card.flip(false).then(() => {
            expect(card.isAnimated).toBe(false);
            done();
          });
        });
      });

      describe('when side is not equal to card side', () => {
        it('should flip side', (done) => {
          card._side = false;
          card.flip(true).then(() => {
            expect(card._side).toBe(true);
            done();
          });
        });
      });
    });
  });
});