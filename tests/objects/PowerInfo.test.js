import TextObject from '@/objects/TextObject';
import PowerInfo from '@/objects/PowerInfo';

describe('PowerInfo', () => {
  it('should be function', () => {
    expect(PowerInfo).toBeFunction();
  });

  it('should be child of TextObject', () => {
    const instance = new PowerInfo(0, 0, 'foobar');
    const parent = Object.getPrototypeOf(instance.constructor);

    expect(parent).toBe(TextObject);
  });
});