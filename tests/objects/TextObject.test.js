import TextObject from '@/objects/TextObject';

describe('TextObject', () => {
  it('should be function', () => {
    expect(TextObject).toBeFunction();
  });

  it('should be child of PIXI.Text', () => {
    const instance = new TextObject(0, 0, 'foobar');
    const parent = Object.getPrototypeOf(instance.constructor);

    expect(parent).toBe(PIXI.Text);
  });
});
