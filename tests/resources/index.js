import dummy from './dummy.jpg';

export default {
  'textures': [
    {
      'name': 'foobar',
      'url': dummy
    },
    {
      'name': 'card/reverse',
      'url': dummy
    }
  ]
};

export {
  dummy
};