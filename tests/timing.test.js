import * as timing from '@/timing';

describe('linear timing', () => {
  describe('when 0 <= x <= 1', () => {
    it('should return proper results', () => {
      expect(timing.linear(0)).toBe(0);
      expect(timing.linear(0.5)).toBe(0.5);
      expect(timing.linear(1)).toBe(1);
    });
  });

  describe('when x < 0 && x > 1', () => {
    it('should return proper results', () => {
      expect(timing.linear(-1)).toBe(0);
      expect(timing.linear(1)).toBe(1);
    });
  });
});